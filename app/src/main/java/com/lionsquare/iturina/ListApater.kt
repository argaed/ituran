package com.lionsquare.iturina

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lionsquare.iturina.data.Loc

class ListApater(
    var arrayList: ArrayList<Loc>?, var context: Context
) : RecyclerView.Adapter<ListApater.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_loc, parent, false)

        return ViewHolder(view);
    }

    override fun getItemCount(): Int {
        return arrayList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvL.text = arrayList!![position].lat.toString()
        holder.tvlng.text = arrayList!![position].lng.toString()

    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvL: TextView = itemView.findViewById(R.id.tv_lt)
        internal var tvlng: TextView = itemView.findViewById(R.id.tv_lgn)


    }
}