package com.lionsquare.demofyline.fragments.login


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.transition.TransitionInflater
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.lionsquare.iturina.BaseFragment
import com.lionsquare.iturina.R
import com.lionsquare.iturina.data.User
import com.lionsquare.iturina.ui.login.LoginActivity
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : BaseFragment(), View.OnClickListener {


    override fun onRestart() {

    }


    companion object {
        var TAG = RegisterFragment.javaClass.simpleName
        fun newInstance(instance: Int): RegisterFragment {
            val args = Bundle()
            args.putInt("value", instance)
            val fragment = RegisterFragment()

            fragment.arguments = args
            return fragment
        }

    }

    internal lateinit var fr_til_name: TextInputLayout
    internal lateinit var fr_til_email: TextInputLayout
    internal lateinit var fr_til_password: TextInputLayout
    internal lateinit var fr_til_val_password: TextInputLayout
    internal lateinit var fr_iv_ic: ImageView

    private var btnRegister: Button? = null
    internal lateinit var fr_relative: RelativeLayout

    internal lateinit var v: View

    override fun layout(): Int {
        return R.layout.fragment_register
    }

    override fun onCreateFragment(savedInstanceState: Bundle?) {
        //activity.setTheme(R.style.AppTheme)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition =
                TransitionInflater.from(mContext).inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateViewFragment(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
        view: View?
    ) {
        v = view!!
        iniView(v)
    }

    fun iniView(view: View?) {

        btnRegister = view!!.findViewById(R.id.fl_btn_registar)
        btnRegister!!.setOnClickListener(this)
        fr_til_email = view!!.findViewById(R.id.fr_til_email)
        fr_til_password = view!!.findViewById(R.id.fr_til_password)
        fr_til_val_password = view!!.findViewById(R.id.fr_til_val_password)
        fr_til_name = view!!.findViewById(R.id.fr_til_name)



    }

    override fun onStart() {
        super.onStart()
        auth = FirebaseAuth.getInstance()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.fl_btn_registar -> {
                auth()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                activity.onBackPressed();
            }
        }
        return true
    }

    private fun auth() {
        val email = fr_til_email.editText!!.text.toString().trim { it <= ' ' }
        val password = fr_til_password.editText!!.text.toString().trim { it <= ' ' }
        val passwordval = fr_til_val_password.editText!!.text.toString().trim { it <= ' ' }
        val name = fr_til_name.editText!!.text.toString().trim { it <= ' ' }


        if (TextUtils.isEmpty(name)) {
            Toast.makeText(context, "Ingresar nombre", Toast.LENGTH_SHORT).show()
            return
        }

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(context, "Enter email address!", Toast.LENGTH_SHORT).show()
            return
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(context, "Ingresar contraseña", Toast.LENGTH_SHORT).show()
            return
        }
        if (TextUtils.isEmpty(passwordval)) {
            Toast.makeText(context, "Ingresar confirmación contraseña", Toast.LENGTH_SHORT).show()
            return
        }

        if (password.length < 6) {
            Toast.makeText(context, "La contraseña debe tener minimo 6 caracteres", Toast.LENGTH_SHORT).show()
            return
        }

        auth!!.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(activity,
                OnCompleteListener { task ->

                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (!task.isSuccessful) {
                        Toast.makeText(
                            context, "Authentication failed." + task.exception!!,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Log.e("getUid", task.result!!.user.uid)
                        val user = auth!!.currentUser
                        user!!.sendEmailVerification()
                        createUser(task.result!!.user.uid)

                    }
                })

    }

    private fun createUser(uid: String) {

        val mDatabase = FirebaseDatabase.getInstance().getReference("users")

        //String userId = mDatabase.push().getKey();
        val timezone = java.util.TimeZone.getDefault().id

        val user = User(
            fr_til_email.editText!!.text.toString().trim(),
            Calendar.getInstance().time.time.toString() + ""
            , fr_til_name.editText!!.text.toString()
        )

        mDatabase.child(uid).setValue(user).addOnCompleteListener { task ->
            if (task.isComplete) {
                val i = Intent(activity, LoginActivity::class.java)
                startActivity(i)
                activity!!.finish()
            } else {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
                Log.e("fail", "" + task.exception)
            }
        }.addOnFailureListener {
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()


        }


    }


}
