package com.lionsquare.iturina.ui.main


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.lionsquare.demofyline.fragments.login.LoginFragment
import com.lionsquare.iturina.BaseFragment

import com.lionsquare.iturina.R
import com.lionsquare.iturina.data.Loc

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MapsFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMapLongClickListener {


    companion object {
        val TAG = MapsFragment.javaClass::class.java.name!!
        fun newInstance(instance: Int): MapsFragment {
            val args = Bundle()
            args.putInt("value", instance)
            val fragment = MapsFragment()

            return fragment
        }
    }


    override fun layout(): Int {
        return R.layout.fragment_maps
    }

    override fun onCreateFragment(savedInstanceState: Bundle?) {

    }

    private var map: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null

    override fun onCreateViewFragment(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
        view: View?
    ) {
        var btnList = view!!.findViewById<Button>(R.id.btn_lista)
        mapFragment = SupportMapFragment.newInstance()
        childFragmentManager!!.executePendingTransactions()
        childFragmentManager!!.beginTransaction().replace(R.id.map, mapFragment!!).commitAllowingStateLoss()
        mapFragment!!.getMapAsync(this)

        btnList.setOnClickListener {

            sectionFragment!!.pushFragment(ListFragment.TAG, ListFragment.newInstance(0))
        }

    }

    override fun onRestart() {

    }

    override fun onStart() {
        super.onStart()
        auth = FirebaseAuth.getInstance()

    }

    override fun onMapReady(map: GoogleMap?) {
        map!!.setOnMapLongClickListener(this)


    }


    override fun onMapLongClick(p0: LatLng?) {

        val mDatabase = FirebaseDatabase.getInstance().getReference("users")
        var latLng = Loc(p0!!.latitude, p0.longitude)
        var list:ArrayList<LatLng> = ArrayList()
        list.add(p0)


        mDatabase.child(auth!!.currentUser!!.uid).child("LatLng").push().setValue(latLng).addOnCompleteListener { task ->
            if (task.isComplete) {
                Toast.makeText(context, "Hecho", Toast.LENGTH_SHORT).show()

            } else {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
                Log.e("fail", "" + task.exception)
            }
        }.addOnFailureListener {
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()

        }

    }




}
