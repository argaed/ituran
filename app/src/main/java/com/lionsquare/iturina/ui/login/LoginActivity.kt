package com.lionsquare.iturina.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.lionsquare.demofyline.fragments.login.LoginFragment
import com.lionsquare.iturina.BaseActivity
import com.lionsquare.iturina.R


class LoginActivity : BaseActivity() {
    override fun pushFragment(tag: String, fragment: Fragment) {
        val fm = supportFragmentManager

        val ft = fm.beginTransaction()
        ft.replace(R.id.container_login, fragment, LoginFragment.TAG)
        ft.commit()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if (savedInstanceState == null) {
            val fm = supportFragmentManager
            val fragment = LoginFragment.newInstance(0)
            val ft = fm.beginTransaction()
            ft.replace(R.id.container_login, fragment, LoginFragment.TAG)
            ft.commit()
        }


    }




}
