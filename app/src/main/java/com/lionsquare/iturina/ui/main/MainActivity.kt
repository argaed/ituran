package com.lionsquare.iturina.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.lionsquare.demofyline.fragments.login.LoginFragment
import com.lionsquare.iturina.BaseActivity
import com.lionsquare.iturina.R

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            val fm = supportFragmentManager
            val fragment = MapsFragment.newInstance(0)
            val ft = fm.beginTransaction()
            ft.replace(R.id.content_main , fragment, MapsFragment.TAG)
            ft.commit()
        }

    }

    override fun pushFragment(tag: String, fragment: Fragment) {
        val fm = supportFragmentManager

        val ft = fm.beginTransaction()
        ft.replace(R.id.content_main, fragment, LoginFragment.TAG)
        ft.commit()
    }

}
