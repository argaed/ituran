package com.lionsquare.iturina.ui.main

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.lionsquare.demofyline.fragments.login.RegisterFragment
import com.lionsquare.iturina.BaseFragment
import com.lionsquare.iturina.ListApater

import com.lionsquare.iturina.R
import com.lionsquare.iturina.data.Loc
import com.lionsquare.iturina.data.User

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ListFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class ListFragment : BaseFragment() {
    override fun layout(): Int {
        return R.layout.fragment_list
    }


    companion object {
        var TAG = ListFragment.javaClass.simpleName
        fun newInstance(instance: Int): ListFragment {
            val args = Bundle()
            args.putInt("value", instance)
            val fragment = ListFragment()

            fragment.arguments = args
            return fragment
        }
    }

    var locList: ArrayList<Loc>? = null
    var rvList: RecyclerView? = null
    var adatper: ListApater? = null
    override fun onCreateFragment(savedInstanceState: Bundle?) {
        auth = FirebaseAuth.getInstance()
    }

    override fun onCreateViewFragment(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
        view: View?
    ) {
        rvList=view!!.findViewById(R.id.rv_list)
        val layoutManager = LinearLayoutManager(getActivity())
        val dividerItemDecoration = DividerItemDecoration(
            rvList!!.context,
            layoutManager.orientation
        )
        rvList!!.addItemDecoration(dividerItemDecoration)
        rvList!!.layoutManager = layoutManager
        locList = arrayListOf()
        val mDatabase = FirebaseDatabase.getInstance().getReference("users")
        mDatabase.child(auth!!.uid!!).child("LatLng").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                for (dataSnapshots in dataSnapshot.children) {

                    val loc: Loc = dataSnapshots.getValue(Loc::class.java)!!
                    Log.e("lat",loc.lat.toString())
                    locList!!.add(loc)

                }
                Log.e("tamaño",locList!!.size.toString())
                adatper = ListApater(locList, context!!)
                rvList!!.adapter = adatper

            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "ocurrio un error ", Toast.LENGTH_SHORT).show()

            }
        })


    }

    override fun onRestart() {

    }

    override fun onStart() {
        super.onStart()

    }

}
