package com.lionsquare.demofyline.fragments.login


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.lionsquare.iturina.BaseFragment
import com.lionsquare.iturina.R
import com.lionsquare.iturina.data.BeanGlobal.Companion.RC_SIGN_IN
import com.lionsquare.iturina.ui.main.MainActivity


/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : BaseFragment(), View.OnClickListener {


    var progressBar: ProgressBar? = null
    override fun layout(): Int {
        return R.layout.fragment_login
    }

    override fun onCreateViewFragment(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
        view: View?
    ) {
        v = view
        initView(v)
    }

    override fun onRestart() {
    }


    companion object {
        val TAG = LoginFragment.javaClass::class.java.name!!
        fun newInstance(instance: Int): LoginFragment {
            val args = Bundle()
            args.putInt("value", instance)
            val fragment = LoginFragment()

            return fragment
        }
    }


    protected var v: View? = null

    protected lateinit var btnEmail: Button
    protected lateinit var btnLogin: Button


    private lateinit var tilEmail: TextInputLayout
    private lateinit var tilPass: TextInputLayout
    private lateinit var acetPass: AppCompatEditText
    private lateinit var fl_coordinator: CoordinatorLayout


    override fun onCreateFragment(savedInstanceState: Bundle?) {


    }

    fun initView(view: View?) {

        fl_coordinator = view!!.findViewById(R.id.fl_coordinator)
        progressBar = view!!.findViewById(R.id.progressBar)
        btnEmail = view!!.findViewById(R.id.fl_btn_email)
        btnLogin = view.findViewById(R.id.fl_btn_login)
        tilEmail = view.findViewById(R.id.fr_til_email)
        tilPass = view.findViewById(R.id.fr_til_password)



        btnEmail.setOnClickListener(this)
        acetPass = view!!.findViewById(R.id.acet_pass)

        btnLogin.setOnClickListener(View.OnClickListener {
            login()
        })

    }


    private fun login() {
        val email = tilEmail.editText!!.text.toString()
        val password = tilPass.editText!!.text.toString()

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(
                context,
                "¡Introducir la dirección de correo electrónico!", Toast.LENGTH_SHORT
            ).show()

            return
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(context, "¡Introducir la contraseña!", Toast.LENGTH_SHORT).show()
            return
        }
        progressBar!!.visibility = View.VISIBLE


        auth!!.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(activity) { task ->

                if (!task.isSuccessful) {
                    progressBar!!.visibility = View.GONE
                    // there was an error
                    if (password.length < 6) {
                        tilPass.editText!!.error = mContext!!.getString(R.string.minimum_password)
                    } else {
                        Toast.makeText(context, mContext!!.getString(R.string.auth_failed), Toast.LENGTH_LONG)
                            .show()
                    }
                } else {
                    progressBar!!.visibility = View.GONE
                    val i = Intent(context, MainActivity::class.java)
                    startActivity(i)
                    activity.finish()
                }


            }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("resultCode", "" + resultCode)

        if (requestCode == RC_SIGN_IN) {

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                handleSignInResult(account!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.e(TAG, "Google sign in failed", e)
                Toast.makeText(mContext, getString(R.string.upps), Toast.LENGTH_SHORT).show()

                // ...
            }
        }
    }


    private fun handleSignInResult(acct: GoogleSignInAccount) {

        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)

        auth!!.signInWithCredential(credential).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information

                Log.d(TAG, "signInWithCredential:success")
                val user = auth!!.currentUser
                val i = Intent(context, MainActivity::class.java)
                startActivity(i)
                activity.finish()
            } else {

                // If sign in fails, display a message to the user.
                Log.w(TAG, "signInWithCredential:failure", task.exception)
            }

            // ...
        }

    }


    override fun onClick(v: View?) {

        when (v!!.id) {

            R.id.fl_btn_email -> {
                sectionFragment!!.pushFragment(RegisterFragment.TAG, RegisterFragment.newInstance(0))

            }


        }
    }

    override fun onStart() {
        super.onStart()
        auth = FirebaseAuth.getInstance()

    }


}
