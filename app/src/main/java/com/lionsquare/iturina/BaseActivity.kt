package com.lionsquare.iturina

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.lionsquare.demofyline.fragments.login.LoginFragment


abstract class BaseActivity : AppCompatActivity(), BaseFragment.SectionFragment {





    protected lateinit var context: Context
    protected lateinit var activity: Activity


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        activity = this
    }


}
