package com.lionsquare.iturina.data

class User {
    var email: String? = null
    var fechaRegistro: String? = null
    var nombre: String? = null

    constructor(email: String, fechaRegistro: String, nombre: String) {
        this.email = email
        this.fechaRegistro = fechaRegistro
        this.nombre = nombre
    }

    constructor() {}
}