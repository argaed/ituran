package com.lionsquare.iturina.data

class Loc {

    var lat: Double? = null
    var lng: Double? = null

    constructor(lat: Double?, lng: Double?) {
        this.lat = lat
        this.lng = lng
    }

    constructor() {}
}
