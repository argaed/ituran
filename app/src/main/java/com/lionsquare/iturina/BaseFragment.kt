package com.lionsquare.iturina


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth


/**
 * A simple [Fragment] subclass.
 *
 */
abstract class BaseFragment : androidx.fragment.app.Fragment() {

    protected var mContext: Context? = null
    protected lateinit var activity: Activity
    protected var auth: FirebaseAuth? = null
    protected var cachedView: View? = null
    protected var sectionFragment: SectionFragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = context
        activity = this!!.getActivity()!!
        retainInstance = true
        //setHasOptionsMenu(true)
        onCreateFragment(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (cachedView == null) {
            cachedView = inflater.inflate(layout(), container, false)
            onCreateViewFragment(inflater, container, savedInstanceState, cachedView)
        } else {
            onRestart()
        }
        return cachedView
    }


    abstract
    fun layout(): Int

    abstract
    fun onCreateFragment(savedInstanceState: Bundle?)

    abstract
    fun onCreateViewFragment(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?, view: View?
    )

    abstract fun onRestart()


    interface SectionFragment {


        fun pushFragment(tag: String, fragment: Fragment)


    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SectionFragment) {
            sectionFragment = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        sectionFragment= null

    }


}